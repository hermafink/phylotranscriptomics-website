function bar_chart(var_test,bind) {
  var executed = false;
  // var test_var = '{{ stages }}';
  console.log(test_var);
  console.log(test_var.length)
  // console.log      console.log(data)('{{ stages }}' );
  var chart = c3.generate({
    bindto: bind,
    data: {
        columns: test_var
          // [test_var[0][0], test_var[0][1]],
          // [test_var[1][0], test_var[1][1]],
        ,
        type: 'bar',
        onclick: function (d, i) {

          $("#modal_form").val(d.id);
          $('#modal_table').trigger('click');
          // if (executed == false){
          //   myFunction();
          //   executed = true;
          // }
          // table.innerHTML = table;
          // table.appendChild(table);;

          // https://stackoverflow.com/questions/8302166/dynamic-creation-of-table-with-dom
          console.log(d.id);
      },
      },
        bar: {
        width: {
          ratio: 1 // this makes bar width 50% of length between ticks
        }
      // or
      //width: 100 // this makes bar width 100px
    }
  });
  return chart;   // The function returns the product of p1 and p2
}

function intactParser(intact){
  var data = "";
  data += '['
  for (i in intact) {
    data += '{ data: { id: "';
    data += intact[i][1]
    data += '" } },'
    // console.log(intact[i]);
  }
  data += ']'
  return data;
};

function cytoscapeGraph(intact) {
  // data = intactParser(intact);
  // console.log(data);
  var cy = cytoscape({
    container: document.getElementById('cy'),
    elements: intact,
    style: [
        {
            selector: 'node',
            style: {
              label: 'data(id)',
              // "font-size": '8px'
        }
      }],
    layout: {
    name: 'random'
}

    });
    cy.resize()
    cy.on('tap', 'node', function(evt){
      var node = evt.target;
      // window.open("https://www.youraddress.com","_self")
      console.log( 'tapped ' + node.id() );
    });
};

function search() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}


$(function() {
  $('#modal_table').click(function(e) {
    $.ajax({
      url: '/descr',
      data: $('form').serialize(),
      type: 'POST',
     success: function(response) {
          $("#place_for_suggestions").html(response);
           console.log(response);
         },
         error: function(error) {
           console.log(error);
         }
       });
     });
   });
  // $('.collapse').collapse("toggle")


// $(function () {
//   $('[data-toggle="tooltip"]').tooltip()
// })

  // function myFunction(){
  //
  //   var table = document.createElement('table');
  //
  //   var tr = document.createElement('tr');
  //   var th1 = document.createElement('th');
  //   var th2 = document.createElement('th');
  //
  //   var head1 = document.createTextNode("Stage");
  //   var head2 = document.createTextNode("Count");
  //
  //   th1.appendChild(head1);
  //   th2.appendChild(head2);
  //
  //   tr.appendChild(th1);
  //   tr.appendChild(th2);
  //
  //   table.appendChild(tr);
  //
  //   {% for entrie in stages %}
  //   var tr = document.createElement('tr');
  //
  //   var td1 = document.createElement('td');
  //   var td2 = document.createElement('td');
  //
  //   var th = document.createElement('th');
  //
  //   var text1 = document.createTextNode("{{ entrie[0] }}");
  //   var text2 = document.createTextNode("{{ entrie[1] }}");
  //
  //   td1.appendChild(text1);
  //   td2.appendChild(text2);
  //   tr.appendChild(td1);
  //   tr.appendChild(td2);
  //   table.appendChild(tr);
  //   {% endfor %}
  //
  //   var x = document.getElementById("test");
  //   x.appendChild(table);
  // };
