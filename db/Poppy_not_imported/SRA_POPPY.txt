Transcriptome analysis of the Eschscholzia californica roots.
SRX096037: Transcriptome analysis of the Eschscholzia californica roots.
1 ILLUMINA (Illumina Genome Analyzer II) run: 31.4M spots, 6.8G bases, 3.9Gb downloads
Submitted by: University of Calgary - Sun Center of Excellence for Visual Genomics (UC-COE)

Transcriptome analysis of the Eschscholzia californica root.
SRX039637: Transcriptome analysis of the Eschscholzia californica root.
1 LS454 (454 GS FLX Titanium) run: 472,240 spots, 245.9M bases, 533.4Mb downloads
Submitted by: University of Calgary - Sun Center of Excellence for Visual Genomics (UC-COE)
https://www.ncbi.nlm.nih.gov/sra/SRX039637[accn]

Eschscholzia californica floral tissue - random primed
SRX002988: Eschscholzia californica floral tissue - random primed
2 LS454 (454 GS 20) runs: 311,917 spots, 35.5M bases, 86.7Mb downloads
Submitted by: Penn State University
https://www.ncbi.nlm.nih.gov/sra/SRX002988[accn]

Eschscholzia californica floral tissue - oligo-dT
SRX002987: Eschscholzia californica floral tissue - oligo-dT
2 LS454 (454 GS 20) runs: 264,205 spots, 30.3M bases, 74.3Mb downloads
Submitted by: Penn State University
https://www.ncbi.nlm.nih.gov/sra/SRX002987[accn]

ERX337103: Population Genomics of California poppy
1 ILLUMINA (Illumina Genome Analyzer II) run: 12.3M spots, 1.8G bases, 792.4Mb downloads
https://www.ncbi.nlm.nih.gov/sra/ERX337103[accn]

ERX337106: Population Genomics of California poppy
1 ILLUMINA (Illumina Genome Analyzer II) run: 13.5M spots, 2G bases, 857.7Mb downloads
https://www.ncbi.nlm.nih.gov/sra/ERX337106[accn]

ERX337105: Population Genomics of California poppy
1 ILLUMINA (Illumina Genome Analyzer II) run: 14M spots, 2.1G bases, 909.4Mb downloads
https://www.ncbi.nlm.nih.gov/sra/ERX337105[accn]

ERX337104: Population Genomics of California poppy
1 ILLUMINA (Illumina Genome Analyzer II) run: 13.9M spots, 2.1G bases, 895.7Mb downloads
https://www.ncbi.nlm.nih.gov/sra/ERX337104[accn]

ERX337107: Population Genomics of California poppy
https://www.ncbi.nlm.nih.gov/sra/ERX337107[accn]



BioSample: SAMEA2242384; SRA: ERS368206

BioSample: SAMEA2242381; SRA: ERS368203
https://www.ncbi.nlm.nih.gov/biosample/2444215

BioSample: SAMEA2242383; SRA: ERS368205
https://www.ncbi.nlm.nih.gov/biosample/2444208


BioSample: SAMEA2242380; SRA: ERS368202
https://www.ncbi.nlm.nih.gov/biosample/2444207

BioSample: SAMEA2242382; SRA: ERS368204
https://www.ncbi.nlm.nih.gov/biosample/2444152

BioSample: SAMN00715836; Sample name: Illumina_ECART2PF; SRA: SRS259979
https://www.ncbi.nlm.nih.gov/biosample/715836


BioSample: SAMN00199199; Sample name: ECARTPF1; SRA: SRS160813
https://www.ncbi.nlm.nih.gov/biosample/199199

BioSample: SAMN00002023; Sample name: Eschscholzia californica; SRA: SRS002203
https://www.ncbi.nlm.nih.gov/biosample/2023