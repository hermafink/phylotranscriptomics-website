import pandas as pd

df = pd.read_csv('network_shrinked.csv', sep ="\t")

print(df)

nodes = ""
edges = ""


for index, row in df.iterrows():
    nodes += '{ data: { id:"'
    nodes += row['Node 1']
    nodes += '"} },'

    nodes += '{ data: { id:"'
    nodes += row['Node 2']
    nodes += '"} },'

    edges +=  '{ data: { id:"'
    edges += row['Node 1']+row['Node 2']
    edges += '", source:"'
    edges += row['Node 1']
    edges += '", target: "'
    edges += row['Node 2']
    edges += '" } },'

print(nodes)
print(edges)


# elements: [
#   { data: { id: 'a' } },
#   { data: { id: 'b' } },
#   { data: { id: 'c' } },
#   { data: { id: 'ab', source: 'a', target: 'b' } },
#   { data: { id: 'ac', source: 'a', target: 'c' } },
#   { data: { id: 'bc', source: 'b', target: 'c' } },
#   ]

# elements: {
#     nodes: data.nodes,
#     edges: data.edges
# },
