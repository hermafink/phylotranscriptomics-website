from flask import Flask, render_template
from flask import request, send_from_directory, jsonify, json,session,flash,redirect,abort
import sqlite3
from flask import g
import subprocess
import pandas as pd
import flask_login
import flask
import uuid
import os

app = Flask(__name__, static_url_path='/static')

app.secret_key = 'EinSuperSichererStringDerKeineAhnungFuerWasBenutztWird' # change

users = {'Hermann': {'password': 'secret'}}

login_manager = flask_login.LoginManager()

login_manager.init_app(app)

class User(flask_login.UserMixin):
    pass

#hallo kleiner Test um zu gucken ob es funktioniert.
# funktioniert es jetzt wenigstens?

@login_manager.user_loader
def user_loader(email):
    if email not in users:
        return

    user = User()
    user.id = email
    return user


@login_manager.request_loader
def request_loader(request):
    # users = query_db('SELECT * FROM USERS')
    email = request.form.get('email')
    if email not in users:
        return

    user = User()
    user.id = email

    # DO NOT ever store passwords in plaintext and always compare password
    # hashes using constant-time comparison!
    user.is_authenticated = request.form['password'] == users[email]['password']

    return user

# @app.route('/')
# # def index():
#     # stages = query_db('SELECT Stage,count(*) FROM test GROUP BY Stage')
#     # pie_s1 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S1"')
#     # pie_s2 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S2"')
#     # pie_s3 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S3"')
#     # pie_s4 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S4"')
#     # tau = query_db('SELECT Tau FROM tau')
#     # return render_template("index.html", stages=stages, pie_s1=pie_s1, pie_s2=pie_s2, pie_s3=pie_s3, pie_s4=pie_s4, tau=tau)# ein test
# def home():
#  if not session.get('logged_in'):
#   return render_template('login.html')
#  else:
#   return "Hello Boss!"

@app.route('/', methods=["GET","POST"])
def home():
    error = ''
    try:

        if request.method == "POST":

            attempted_stage = request.form['stage']
            stage = query_db('SELECT gene_id,Name FROM ara_annotation WHERE Name LIKE ?',["%" + attempted_stage + "%"])
            if not stage:
                stage = query_db('SELECT gene_id,Name FROM ara_annotation WHERE gene_id LIKE ?',["%" + attempted_stage + "%"])
            print(stage)

        return render_template("list.html", stage = stage)

    except Exception as e:
        #flash(e)
        return render_template("home.html", error = error)

# wo liegt der fehler?
@app.route("/species")
def species():
    return render_template("species.html")

@app.route("/arabidopsis")
def arabidopsis():
    stages = query_db('SELECT Stage,count(*) FROM ara_absent_present GROUP BY Stage')
    return render_template("single_species.html", stages=stages)

# Working login from flask_login
@app.route('/login', methods=['GET', 'POST'])
# def login():
#     error = None
#     if request.method == 'POST':
#         if flask.request.form['password'] == users[email]['password']:
#             user = User()
#             user.id = email
#             flask_login.login_user(user)
#             return flask.redirect(flask.url_for('protected'))
#         else:
#             error = 'Invalid username/password'
#     # the code below is executed if the request method
#     # was GET or the credentials were invalid
#     return render_template('login.html', error=error)
def do_admin_login():
    if request.method == 'POST':
        if request.form['password'] == 'password' and request.form['username'] == 'admin':
            session['logged_in'] = True
        else:
            flash('wrong password!')

        return home()

# logout route to logout user
@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

# def login():
#     # if flask.request.method == 'GET':
#     #     return '''
#     #            <form action='login' method='POST'>
#     #             <input type='text' name='email' id='email' placeholder='email'/>
#     #             <input type='password' name='password' id='password' placeholder='password'/>
#     #             <input type='submit' name='submit'/>
#     #            </form>
#     #            '''
#     email = flask.request.form['email']
#     if flask.request.form['password'] == users[email]['password']:
#         user = User()
#         user.id = email
#         flask_login.login_user(user)
#
#         return flask.redirect(flask.url_for('protected'))
#
#     else:
#         return 'Bad login'

# protected route only for logged in user
@app.route('/protected')
@flask_login.login_required
def protected():
    return 'Logged in as: ' + flask_login.current_user.id

# @app.route('/logout')
# def logout():
#     flask_login.logout_user()
#     return 'Logged out'

@login_manager.unauthorized_handler
def unauthorized_handler():
    return 'Unauthorized'

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

DATABASE = '/home/herms/Git/phylotranscriptomics-website/db/phylo.db'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


# Overview für das Gespräch
@app.route('/start', methods=['GET', 'POST'])
def start():
    return render_template("start.html")# ein test

# Graph für Tau-Werte
@app.route('/tau')
def tau():
    tau = query_db('SELECT Tau,S1,S2,S3,S4 FROM tau')
    return render_template("tau.html", tau=tau)# ein test


@app.route('/list')
def list():
    stage = query_db('select Name,Stage from test')
    return render_template("list.html", stage=stage)
    # return jsonify(stages)

# Dyanmic created list
@app.route('/list_dyn')
# @flask_login.login_required
def list_dyn():
    stages = query_db('SELECT Stage,count(*) FROM ara_absent_present GROUP BY Stage')
    # pie_s1 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S1"')
    # pie_s2 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S2"')
    # pie_s3 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S3"')
    # pie_s4 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S4"')
    # tau = query_db('SELECT Tau FROM tau')
    return render_template("list_dyn.html", stages=stages)# ein test

# Working tabs
@app.route('/tabs')
def tabs():
    stages = query_db('SELECT Stage,count(*) FROM test GROUP BY Stage')
    return render_template("tabs.html", stages=stages)

# AJAX Call??
@app.route('/list_dyn_interactive', methods=['POST'])
def list_dyn_interactive():
    user =  request.form['username'];
    # text = request.args.get('jsdata')
    #  attempted_stage = request.form['stage']
    # user = query_db('select * from users where username = ?',
    # [the_username], one=True)
    return json.dumps({'status':'OK','user':user});

@app.route('/descr', methods=['POST'])
def descrGene():
    user =  request.form['username'];
    stage = query_db('SELECT Stage,Gene_ID FROM ara_absent_present WHERE Stage = ?',[user])
    return render_template("list_dyn_interactive.html", stage = stage)

@app.route('/transdev')
def transdev():
    return render_template("transdev.html")

# Pie Chart, can be removed ( I think? )
@app.route('/pie')
def pie():
    pie_s1 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S1"')
    pie_s2 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S2"')
    pie_s3 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S3"')
    pie_s4 = query_db('SELECT InStage,Count FROM pie WHERE Stage="S4"')
    test = query_db('SELECT Stage,count(*) FROM test GROUP BY Stage')
    return render_template("pie.html", pie_s1=pie_s1, pie_s2=pie_s2, pie_s3=pie_s3, pie_s4=pie_s4,test=test)

# Absent Present Bar Chart
@app.route('/appr/')
def appr():
    test = query_db('SELECT Stage,count(*) FROM test GROUP BY Stage')
    heatmap = query_db('SELECT S1,S2,S3,S4 FROM test LIMIT 10')
    return render_template("appr.html", test=test,heatmap=heatmap)

# Dip into Cytoscape.js. Working with extern Python script to generate Data
@app.route('/cytoscape')
def cytoscape():
    return render_template("cytoscape.html")

@app.route('/intact/<name2>')
def intact(name2):
    intact = query_db('SELECT * FROM ara_interaction WHERE protein1 LIKE ? ORDER BY combined_score ASC LIMIT 30',["%" + name2 + "%"]) # AT1G69180
    data = []
    for i in intact:
        print(i)
        print(i[0])
        data.append({
            'data' : { 'id' : i[0] }
            # 'data' : { 'id' : i[1] },
            # 'data' : { 'source' : i[0], 'target': i[1] }
        })

        data.append({
            'data' : { 'id' : i[1] }
            # 'data' : { 'id' : i[1] },
            # 'data' : { 'source' : i[0], 'target': i[1] }
        })

        data.append({
            # 'data' : { 'id' : i[0] }
            # 'data' : { 'id' : i[1] },
            'data' : { 'source' : i[0], 'target': i[1], 'score' : i[2] }
        })



                          # data: {
                  #   id: 'ab',
                  #   source: 'a',
                  #   target: 'b'
                  # }
    print(data)
    return render_template("intact.html", intact=intact,data=data)

# Dynamically generate Steckbrief for Gene Names



@app.route('/name/<name>', methods=['GET'])
def gene_name(name):
    uuid_var = str(uuid.uuid4())
    gene = query_db('SELECT Count FROM ara_annotation WHERE gene_id = ?',[name])
    print(gene)
    list = [gene[0],"\t",name]

    with open('/home/herms/Git/phylotranscriptomics-website/compute/cluster/ids-' + uuid_var + '.csv', 'w') as f:
        for item in list:
            f.write("%s" % item)
    subprocess.call ("/usr/bin/Rscript --vanilla /home/herms/Git/phylotranscriptomics-website/compute/cluster/cluster.R " + uuid_var, shell=True)
    # query_db('mode csv')

    # HEATMAP ERSTELLUNG EXTREMS GEFRANKENSTEINED
    # DATEN WERDEN IN DIE DATENBANK REINGELADEN UND VON DER DATENBANK IN DIE WEBSEITE GELADEN.
    # NICHT ANWENDBAR BEI MEHREREN NUTZERN! KRITISCHER PUNKT

    with sqlite3.connect(DATABASE) as con:
      cur = con.cursor()
      # cur.execute("INSERT INTO students (name) VALUES (?)",(name))
      df = pd.read_csv("/home/herms/Git/phylotranscriptomics-website/compute/cluster/output-" + uuid_var + ".csv", sep="\t")
      df.to_sql("r_output-" + uuid_var, con, if_exists='replace', index=False)
      con.commit()

    heatmap3 = query_db('SELECT "Unnamed: 0",round("Unnamed: 1",5),round(S1,2),round(S2,2),round(S3,2),round(S4,2) FROM "r_output-' + uuid_var + '" ORDER BY "Unnamed: 1" ASC LIMIT 30')
    heatmap4 = query_db('SELECT "Unnamed: 0",round("Unnamed: 1",5),round(S1,2),round(S2,2),round(S3,2),round(S4,2) FROM "r_output-' + uuid_var + '" ORDER BY "Unnamed: 1" DESC LIMIT 30')
    heatmap = heatmap3 + heatmap4
    heatmap1 = query_db('SELECT S1,S2,S3,S4 FROM "r_output-' + uuid_var + '" ORDER BY "Unnamed: 1" DESC LIMIT 30')
    heatmap2 = query_db('SELECT S1,S2,S3,S4 FROM "r_output-' + uuid_var + '" ORDER BY "Unnamed: 1" ASC LIMIT 30')
    yab1 = heatmap1 + heatmap2


    stage = query_db('SELECT * FROM ara_annotation WHERE gene_id = ?',[name])
    # tau = query_db('SELECT * FROM tau WHERE Name = ?',[name])
    de = query_db('SELECT * from ara_de WHERE Name = ? LIMIT 30',[name])

    query_db('DROP TABLE "r_output-' + uuid_var + '"')
    os.remove("/home/herms/Git/phylotranscriptomics-website/compute/cluster/output-" + uuid_var + ".csv")
    os.remove("/home/herms/Git/phylotranscriptomics-website/compute/cluster/ids-" + uuid_var + ".csv")

    intact = query_db('SELECT * FROM ara_interaction WHERE protein1 LIKE ? ORDER BY combined_score ASC LIMIT 30',["%" + name + "%"]) # AT1G69180
    data = []
    for i in intact:
        print(i)
        print(i[0])
        data.append({
            'data' : { 'id' : i[0] }
            # 'data' : { 'id' : i[1] },
            # 'data' : { 'source' : i[0], 'target': i[1] }
        })

        data.append({
            'data' : { 'id' : i[1] }
            # 'data' : { 'id' : i[1] },
            # 'data' : { 'source' : i[0], 'target': i[1] }
        })

        data.append({
            # 'data' : { 'id' : i[0] }
            # 'data' : { 'id' : i[1] },
            'data' : { 'source' : i[0], 'target': i[1], 'score' : i[2] }
        })
        print(data)

    # with sqlite3.connect(DATABASE) as con:
    #   cur = con.cursor()
    #   # cur.execute("INSERT INTO students (name) VALUES (?)",(name))
    #
    #   dropTableStatement = "DROP TABLE r_output-" + uuid_var + ";"
    #   cur.execute(dropTableStatement)
    #   # cur.execute('DROP TABLE ?', ["r_output-" + uuid_var])
    #   con.commit()

    return render_template("descr.html", name=name, stage=stage, title=name,  yab1=yab1, heatmap=heatmap, de=de, data=data, intact=intact)# de=de,tau=tau,

# Heatmap creation with R script on server
@app.route('/heatmap')
def heatmap():
    heatmap = query_db('SELECT S1,S2,S3,S4 FROM test LIMIT 10')
    return render_template("heatmap.html", heatmap=heatmap)

# Search Database and return another site with results
@app.route('/search/', methods=["GET","POST"])
def search():

    error = ''
    try:

        if request.method == "POST":

            attempted_stage = request.form['stage']
            stage = query_db('SELECT InStage,Count FROM pie WHERE Stage = ?',[attempted_stage])

        return render_template("list.html", stage = stage)

    except Exception as e:
        #flash(e)
        return render_template("search.html", error = error)

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
